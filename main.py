import asyncio
import json
import logging
import os
from asyncio import TimeoutError
from datetime import datetime
from functools import reduce

import aiohttp
import tiktoken
from aiohttp import ClientTimeout, ClientError
from aiohttp import ContentTypeError
from bs4 import BeautifulSoup, NavigableString, Tag
from duckduckgo_search import AsyncDDGS
from httpx import HTTPError
from openai import AsyncClient
from openai.types.chat import ChatCompletionMessage

your_brave_api_key = None
your_openai_api_key = None

brave_api_key = os.getenv("BRAVE_API_KEY", your_brave_api_key)
openai_api_key = os.getenv("OPENAI_API_KEY", your_openai_api_key)

openai_client = AsyncClient(api_key=openai_api_key, timeout=60.0)

formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def core_system_prompt() -> str:
    return f'''
You are an autonomous agent, specialized in retrieving and analysing data.
Given some contextual information and access to various data sources, provide a precise and comprehensive analysis.
Be very thorough and exhaustive when retrieving information.
Always consult various sources and cross-reference your findings.
If you consult a specific data source multiple times, use differnt queries in the appropriate language for each request.
Think step by step and base your answer on the retrieved information, but try to make educated guesses where applicable.
Today is {datetime.now().strftime("%d/%m/%Y ))")}.'''


def medical_system_prompt() -> str:
    return f'''
You are an autonomous agent, specialized in retrieving and analysing medical data.
Use the relevant OpenFDA API to get reliable information on drugs, devices or establishments.
Try a web search if the OpenFDA API does not provide what the user is looking for, or to get current news (like shortages or disruptions).
Be very thorough and exhaustive and prioritize authoritative sources (like FDA or BfArM) when retrieving information from the web.
If you consult a specific data source multiple times, use differnt queries in the appropriate language for each request.
Avoid giving medical or diagnostic advice that you are not qualified to provide.
Think step by step and make sure your responses are comprehensive, accurate and concise.
When asked to provide or resolve coordinates, answer as best as you can.
Answer only if you know the answer or can make a well-informed guess; otherwise say you don't know.
Today is {datetime.now().strftime("%d/%m/%Y ))")}.'''


def summary_user_prompt(content: str) -> str:
    return f'''
Summarize and keep relevant URLs: """{content}"""'''


def art_choice() -> str:
    return f'''
.--.      .--.      .--.      .--.      .--.      .--.      .--.      
:::.\::::::::.\::::::::.\::::Action:::::::.\::::::::.\::::::::.\::::
     `--'      `--'      `--'      `--'      `--'      `--'      `--' '''


def art_initial() -> str:
    return f'''
.--.      .--.      .--.      .--.      .--.      .--.      .--.      
:::.\::::::::.\::::::::.\::Initializing::::.\::::::::.\::::::::.\::::
     `--'      `--'      `--'      `--'      `--'      `--'      `--' '''


def art_final() -> str:
    return f'''
.--.      .--.      .--.      .--.      .--.      .--.      .--.      
:::.\::::::::.\::::::::.\:::::Stop:::::::.\::::::::.\::::::::.\::::
     `--'      `--'      `--'      `--'      `--'      `--'      `--' '''


async def click_link(url: str):
    logger.debug(f'Tool | Click URL - "{url}"')

    def traverse_html(soup, snippets: list, selector='') -> None:
        block_tags = [
            "address",
            "article",
            "aside",
            "blockquote",
            "canvas",
            "dd",
            "div",
            "dl",
            "dt",
            "fieldset",
            "figcaption",
            "figure",
            "footer",
            "form",
            "h1",
            "h2",
            "h3",
            "h4",
            "h5",
            "h6",
            "header",
            "hr",
            "li",
            "main",
            "nav",
            "noscript",
            "ol",
            "p",
            "pre",
            "section",
            "table",
            "tfoot",
            "ul",
            "video"]

        if isinstance(soup, NavigableString):
            if soup.parent.name in block_tags and not soup.next_sibling or soup.name in block_tags:
                snippets.append((soup.text + "\n", selector))

            else:
                snippets.append((soup.text, selector))

        elif soup is not None:
            child_selector = selector

            for child in soup.children:
                if child.name:
                    nth = sum(1 for _ in child.previous_siblings if isinstance(_, Tag)) + 1
                    child_selector = f"{selector} > {child.name}:nth-child({nth})"
                    traverse_html(child, snippets, selector=child_selector)

                else:
                    traverse_html(child, snippets, selector=child_selector)

    def remove_style_elements(soup):
        style_tags = soup.find_all('style')

        for style_tag in style_tags:
            style_tag.extract()

        return soup

    timeout = ClientTimeout(total=3)
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}

    async with aiohttp.ClientSession(headers=headers, timeout=timeout) as session:
        try:
            async with session.post(url=url) as response:
                if response.status == 200:
                    html_content = await response.text()

                else:
                    html_content = f"{response.status} - {response.reason}"

        except TimeoutError:
            html_content = "Timeout - do NOT retry this URL"

        except ClientError as e:
            html_content = f"Error: {e}"

    html_soup = remove_style_elements(BeautifulSoup(html_content, "html.parser"))
    html_snippets = []
    traverse_html(html_soup, html_snippets)

    return reduce(lambda x, y: x + " " + y[0], html_snippets, '').strip().replace("\n\n\n", "")


async def search_news(query: str, max_results: int = 10):
    logger.debug(f'Tool | News Search API - "{query}"')

    async with AsyncDDGS() as ddgs:
        return [r async for r in ddgs.news(keywords=query, max_results=max_results)]


async def search_web(query: str, max_results: int = 7):
    logger.debug(f'Tool | Web Search API - "{query}"')

    url = "https://api.search.brave.com/res/v1/web/search"
    params = {'q': query, 'count': max_results, 'extra_snippets': 'True'}
    headers = {
        "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "X-Subscription-Token": brave_api_key}

    async with aiohttp.ClientSession() as session:
        async with session.get(url, params=params, headers=headers) as response:
            if response.status == 200:
                results = await response.json()
                pruned_results = [{
                    'title': result.get('title', ""),
                    'url': result.get('url', ""),
                    'extra_snippets': result.get('extra_snippets', []),
                } for result in results['web']['results']]

                return pruned_results

            else:
                return f"{response.status} - {response.reason}"


async def fda_drugs(search: str, limit: int = 5, skip: int = 0):
    logger.debug(f'Tool | OpenFDA Drug API - "{search}"')

    url = "https://api.fda.gov/drug/ndc.json"
    params = {'search': search, 'limit': limit, 'skip': skip}

    async with aiohttp.ClientSession() as session:
        async with session.get(url, params=params) as response:
            if response.status == 200:
                return await response.json()

            else:
                return f"{response.status} - {response.reason}"


async def fda_devices(search: str, limit: int = 5, skip: int = 0):
    logger.debug(f'Tool | OpenFDA Devices API - "{search}"')

    url = "https://api.fda.gov/device/registrationlisting.json"
    params = {'search': search, 'limit': limit, 'skip': skip}

    async with aiohttp.ClientSession() as session:
        async with session.get(url, params=params) as response:
            if response.status == 200:
                return await response.json()

            else:
                return f"{response.status} - {response.reason}"


def core_functions() -> dict:
    return {"search_web": search_web, "search_news": search_news, "click_link": click_link}


def core_messages() -> list:
    return [{"role": "system", "content": core_system_prompt().lstrip()}]


def core_tools() -> list:
    return [
        {
            "type": "function",  # Brave web search
            "function": {
                "name": "search_web",
                "description": "Perform a web search, always use as fallback if other tools provide insufficient data",
                "parameters": {
                    "type": "object",
                    "properties": {
                        "query": {
                            "type": "string",
                            "description": "The search query"
                        },
                    },
                    "required": ["query"],
                },
            },
        },
        {
            "type": "function",  # DuckDuckGo news search
            "function": {
                "name": "search_news",
                "description": "Get up-to-date news about a topic",
                "parameters": {
                    "type": "object",
                    "properties": {
                        "query": {
                            "type": "string",
                            "description": "The search topic, prefer short and concise keywords"
                        },
                    },
                    "required": ["query"],
                },
            },
        },
        {
            "type": "function",  # click URL
            "function": {
                "name": "click_link",
                "description": "Retrieve information from a web resource, only use if you are very sure to have the correct URL",
                "parameters": {
                    "type": "object",
                    "properties": {
                        "url": {
                            "type": "string",
                            "description": "The URL to retrieve information from"
                        },
                    },
                    "required": ["url"],
                },
            },
        },
    ]


def medical_functions() -> dict:
    return {
        "search_web": search_web,
        "search_news": search_news,
        "click_link": click_link,
        "fda_drugs": fda_drugs,
        "fda_devices": fda_devices}


def medical_messages() -> list:
    return [{"role": "system", "content": medical_system_prompt().lstrip()}]


def medical_tools() -> list:
    return [
        {
            "type": "function",  # Brave web search
            "function": {
                "name": "search_web",
                "description": "Perform a web search, always use as fallback if other tools provide insufficient data",
                "parameters": {
                    "type": "object",
                    "properties": {
                        "query": {
                            "type": "string",
                            "description": "The search query"
                        },
                    },
                    "required": ["query"],
                },
            },
        },
        {
            "type": "function",  # DuckDuckGo news search
            "function": {
                "name": "search_news",
                "description": "Get up-to-date news about a topic",
                "parameters": {
                    "type": "object",
                    "properties": {
                        "query": {
                            "type": "string",
                            "description": "The search topic, prefer short and concise keywords"
                        },
                    },
                    "required": ["query"],
                },
            },
        },
        {
            "type": "function",  # click URL
            "function": {
                "name": "click_link",
                "description": "Retrieve information from a web resource, only use if you are very sure to have the correct URL",
                "parameters": {
                    "type": "object",
                    "properties": {
                        "url": {
                            "type": "string",
                            "description": "The URL to retrieve information from"
                        },
                    },
                    "required": ["url"],
                },
            },
        },
        {
            "type": "function",  # OpenFDA drugs search
            "function": {
                "name": "fda_drugs",
                "description": "Search drug establishment and packaging records",
                "parameters": {
                    "type": "object",
                    "properties": {
                        "search": {
                            "type": "string",
                            "description": "The search query",
                        },
                        "limit": {
                            "type": "integer",
                            "description": "Number of records to return",
                        },
                        "skip": {
                            "type": "integer",
                            "description": "Number of records to skip, use for step-by-step pagination",
                        },
                    },
                    "required": ["query"],
                }
            },
        },
        {
            "type": "function",  # OpenFDA devices search
            "function": {
                "name": "fda_devices",
                "description": "Search device establishment records",
                "parameters": {
                    "type": "object",
                    "properties": {
                        "search": {
                            "type": "string",
                            "description": "The search query",
                        },
                        "limit": {
                            "type": "integer",
                            "description": "Number of records to return",
                        },
                        "skip": {
                            "type": "integer",
                            "description": "Number of records to skip, use for step-by-step pagination",
                        },
                    },
                    "required": ["query"],
                }
            },
        },
    ]


def count_tokens(content: str) -> int:
    return len(tiktoken.encoding_for_model("gpt-3.5-turbo").encode(content))


def count_message_tokens(messages: list) -> int:
    encoding = tiktoken.encoding_for_model("gpt-3.5-turbo")
    token_count = 0

    if messages:
        for message in messages:
            token_count += 3

            if isinstance(message, ChatCompletionMessage):
                token_count += len(encoding.encode(str(message.content)))

            elif isinstance(message, dict):
                token_count += len(encoding.encode(str(message["content"])))

    return token_count


async def agent(
        client: AsyncClient,
        layers: int,
        messages: list,
        model: str,
        request: str,
        temperature: float,
        tool_functions: dict,
        tool_specifications: list,
):
    logger.info(art_initial())

    timer = datetime.now()
    response = None
    tokens = 0

    messages.append({"role": "user", "content": request})

    while layers > 0:
        layers -= 1

        if count_message_tokens(messages=messages) > 15000:
            break

        choice = await client.chat.completions.create(
            tools=tool_specifications,
            tool_choice="auto",
            messages=messages,
            model=model,
            # response_format={"type": "json_object"},
            temperature=temperature)

        tokens += choice.usage.total_tokens
        choice = choice.choices[0].message
        messages.append(choice)
        logger.info(art_choice())

        if choice.tool_calls:
            for tool_call in choice.tool_calls:
                function_name = tool_call.function.name
                function_args = json.loads(tool_call.function.arguments)
                function_to_call = tool_functions[function_name]

                try:
                    function_response = str(await function_to_call(**function_args))

                except TypeError as e:
                    logger.error(f'Error | {function_name} - {e}')
                    function_response = f"Error: {e}"

                except ContentTypeError as e:
                    logger.error(f'Error | {function_name} - {e}')
                    function_response = f"Error: {e}"

                except HTTPError as e:
                    logger.error(f'Error | {function_name} - {e}')
                    function_response = f"Error: {e}"

                except UnicodeDecodeError as e:
                    logger.error(f"Error | {function_name} - {e}")
                    function_response = f"Error: {e}"

                logging_response = function_response.replace("\n", "")[:1000]

                if count_tokens(content=function_response) > 3000:
                    logger.debug(f'Pruning | {logging_response}')

                    for i in range(0, len(function_response), 500):
                        if count_tokens(content=function_response[:i + 500]) > 14000:
                            pruned_response = function_response[:i]
                            break

                    else:
                        pruned_response = function_response

                    summary = await client.chat.completions.create(
                        messages=[{"role": "system", "content": summary_user_prompt(content=pruned_response)}],
                        model=model,
                        temperature=0.3)

                    tokens += summary.usage.total_tokens
                    function_response = summary.choices[0].message.content

                    logger.debug(f'Summary | {function_response}')

                else:
                    logger.debug(f'Response | {logging_response}')

                messages.append({"role": "tool", "tool_call_id": tool_call.id, "name": function_name, "content": function_response})

        else:
            response = choice.content
            break

    if not response:
        response = await client.chat.completions.create(
            messages=messages,
            model=model,
            # response_format={"type": "json_object"},
            temperature=temperature)

        tokens += response.usage.total_tokens
        response = response.choices[0].message.content

    response_time = round((datetime.now() - timer).total_seconds(), 2)
    logger.debug(f'Usage | {tokens} tokens / {response_time}s')
    logger.debug(f"Input | {request}")
    logger.debug(f"Output | {response}")
    logger.info(art_final())

    return response


if __name__ == "__main__":
    llm_model = 'gpt-3.5-turbo'  # fast & cheap
    # llm_model = 'gpt-4-turbo-preview'  # smart & expensive
    llm_temperature = 0.4  # 0.2 - 0.7 (default: 0.4)
    evaluation_layers = 5  # 2 - 15 (default: 5)

    core_request = "What are the top 5 bars in Chiang Mai?"
    asyncio.run(agent(
        client=openai_client,
        layers=evaluation_layers,
        messages=core_messages(),
        model=llm_model,
        request=core_request,
        temperature=llm_temperature,
        tool_functions=core_functions(),
        tool_specifications=core_tools()))

    medical_request = "Is Lidocaine an essential medicine?"
    asyncio.run(agent(
        client=openai_client,
        layers=evaluation_layers,
        messages=medical_messages(),
        model=llm_model,
        request=medical_request,
        temperature=llm_temperature,
        tool_functions=medical_functions(),
        tool_specifications=medical_tools()))
