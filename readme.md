# Agent Prototypes

Simple autonomous agent protoypes. 

Set `your_openai_api_key` and select an `llm_model` for LLM access. 
Additionally set `your_brave_api_key` (Brave *Data for AI* API recommended) for web access. 

Specify your `request` and run by executing `main`.

## Core

- Brave Web Search + *Click URL* Tool
- Duck Duck Go News Search

## Medical

- Brave Web Search + *Click URL* Tool
- Duck Duck Go News Search
- OpenFDA API (Drugs & Devices)

## CoyPu AskAnything

- [Documentation](https://docs.coypu.org/APIs.html#coypu--ask-anything)
- [Code](https://gitlab.com/coypu-project/tools/api/-/tree/main/ask)
